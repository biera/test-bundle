<?php

namespace Biera\TestBundle;

use Symfony\Bundle\FrameworkBundle\Client as BaseClient;

class Client extends BaseClient
{
    public function put($uri)
    {
        return $this->fixedClient('PUT', $uri);
    }

    public function patch($uri)
    {
        return $this->fixedClient('PATCH', $uri);
    }

    public function post($uri)
    {
        return $this->fixedClient('POST', $uri);
    }

    /**
     * Get request builder
     *
     * actually it isn't a real builder as it does not return request object
     * instead, it prepares parameters for Client::request method
     *
     * @param $uri
     *
     * @return object
     */
    private function fixedClient($method, $uri)
    {
        return new class($this, $method, $uri)
        {
            private $client;
            private $uri;
            private $method;
            private $headers = [];
            private $parameters = [];
            private $files = [];
            private $server = [];
            private $content = [];
            private $changeHistory = true;

            public function __construct($client, $method, $uri)
            {
                $this->client = $client;
                $this->uri = $uri;
                $this->method = $method;
            }

            public function withHeaders($headers)
            {
                $this->headers = $headers;


                return $this;
            }

            public function withContent($content)
            {
                $this->content = $content;

                return $this;
            }

            /**
             * @return \Symfony\Component\DomCrawler\Crawler
             */
            public function send()
            {
                $this->server = array_merge(
                    $this->server,
                    array_flip(
                        array_map(
                            function($header) {
                                $normalized = strtoupper(str_replace('-', '_', $header));

                                // this header is treated differently by Symfony and must not be prefixed with HTTP_
                                if ('CONTENT_TYPE' === $normalized) {
                                    return $normalized;
                                }

                                return 'HTTP_'.$normalized;
                            },
                            array_flip($this->headers)
                        )
                    )
                );

                return $this->client->request($this->method, $this->uri, $this->parameters, $this->files, $this->server, $this->content, $this->changeHistory);
            }
        };
    }
}
