<?php
namespace Biera\TestBundle\Fixtures;

use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Yaml\Yaml;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\Persistence\ObjectManager;
use Biera\Instantiator;

/**
 * Loads data fixtures from yaml files.
 *
 * @author Jakub Biernacki <kubiernacki@gmail.com>
 */
abstract class YamlFixturesLoader extends AbstractFixture implements ContainerAwareInterface
{
    const CLASS_NOT_FOUND_ERROR_CODE = 600;

    /**
     * @var string|null
     */
    protected $className;

    /**
     * @var boolean
     */
    protected $saveReference = true;

    /**
     * @var boolean
     */
    protected $autoFlush = true;

    /**
     * @var ContainerInterface
     */
    protected $container;

    /**
     * Map of class name to class reflection object
     *
     * @var \ReflectionClass[]
     */
    private $classReflections = [];

    /**
     * {@inheritDoc}
     */
    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager)
    {
        foreach ($this->getDataFixtures() as $objectReferenceName => $objectProperties) {

            $this->preInstantiate($objectProperties);
            $modelObject = $this->instantiateObject($this->getModelClass(), $objectProperties);
            $this->postInstantiate($modelObject);

            $manager->persist($modelObject);

            if ($this->saveReference) {
                $this->addReference($objectReferenceName, $modelObject);
            }

            $this->postLoad($modelObject);
        }

        if ($this->autoFlush) {
            $manager->flush();
        }

    }

    /**
     * Get fully qualified name of model class
     *
     * @return string fully qualified name of model class
     */
    abstract protected function getModelClass();

    /**
     * Pre-instantiate hook
     *
     * @param array $params
     */
    protected function preInstantiate(&$params)
    {
    }

    /**
     * Post-instantiate hook
     *
     * @param object
     */
    protected function postInstantiate($object)
    {
    }

    /**
     * Execute custom post load callback
     *
     * @param object $object
     */
    protected function postLoad($object)
    {
    }

    /**
     * Parse yaml file and return array of fixtures
     *
     * @return array
     */
    protected function getDataFixtures()
    {
        $modelClass = $this->getModelClass();

        try {
            $modelClassReflection = $this->getClassReflection($modelClass);

            $resourceAbsoultePath = $this->container->get('kernel')->locateResource(sprintf('%s/Resources/fixtures/%s.yml', $this->getCurrentBundle(), strtolower(str_replace($modelClassReflection->getNamespaceName() . '\\', '', $modelClassReflection->getName()))));

            return empty($fixtures = Yaml::parse(file_get_contents($resourceAbsoultePath))) ? [] : $fixtures;
        }  catch(\ReflectionException $reflectionException) {
            throw new \RuntimeException(sprintf('Can not find %s model class', $modelClass), self::CLASS_NOT_FOUND_ERROR_CODE, $reflectionException);
        }
    }

    /**
     * Get current bundle name
     *
     * @return string
     *
     * @throws \RuntimeException
     */
    protected function getCurrentBundle()
    {
        $currentBundle = '@';

        foreach (explode('\\', is_null($this->className) ? $this->getModelClass() : $this->className) as $namespaceComponent)
        {
            $currentBundle .= $namespaceComponent;

            if (false !== strpos($namespaceComponent, 'Bundle')) {
                return $currentBundle;
            }
        }

        throw new \RuntimeException(sprintf('Cannot determine current bundle based on %s model class', $this->getModelClass()));
    }

    private function instantiateObject($class, array $params)
    {
        return $this->getInstantiator()->instantiate($class, $params);
    }

    protected function getInstantiator()
    {
        return new Instantiator();
    }

    private function getClassReflection($className)
    {
        if (!array_key_exists($className, $this->classReflections)) {
            $this->classReflections[$className] = new \ReflectionClass($className);
        }

        return $this->classReflections[$className];
    }
}