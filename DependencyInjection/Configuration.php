<?php

namespace Biera\TestBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;


class Configuration implements ConfigurationInterface
{
    /**
     * {@inheritDoc}
     */
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder();
        $rootNode = $treeBuilder->root('biera_test');

        // TODO: do it better!
        $rootNode
            ->fixXmlConfig('fixture_executor')
            ->children()
                ->arrayNode('fixture_executors')
                    ->children()
                        ->arrayNode('odm')
                            ->defaultValue(['default' => 'doctrine_mongodb.odm.default_document_manager'])
                            ->prototype('scalar')->end()
                        ->end()
                        ->arrayNode('orm')
                            ->defaultValue(['default' => 'doctrine.orm.entity_manager'])
                            ->prototype('scalar')->end()
                        ->end()
                    ->end()
                ->end()
            ->end();


        return $treeBuilder;
    }
}
