<?php

namespace Biera\TestBundle\DependencyInjection;

use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\HttpKernel\DependencyInjection\Extension;
use Symfony\Component\DependencyInjection\Loader;
use Symfony\Component\DependencyInjection\Definition;
use Symfony\Component\DependencyInjection\Reference;
use Symfony\Component\DependencyInjection\Parameter;

class BieraTestExtension extends Extension
{
    /**
     * {@inheritDoc}
     */
    public function load(array $configuration, ContainerBuilder $container)
    {
        $processedConfiguration = $this->processConfiguration(new Configuration(), $configuration);
        $loader = new Loader\YamlFileLoader($container, new FileLocator(__DIR__.'/../Resources/config'));
        $loader->load('services.yml');

        if (array_key_exists('fixture_executors', $processedConfiguration) && !empty($processedConfiguration['fixture_executors'])) {
            $executorFactoryDefinition = $container->getDefinition('biera_test.fixtures_executor_factory');

            foreach ($processedConfiguration['fixture_executors'] as $executorType => $executorNames) {
                foreach ($executorNames as $executorName => $objectManagerServiceId) {
                    $executorFactoryDefinition->addMethodCall('addExecutor', [ $executorType, $executorName, $objectManagerServiceId]);
                }
            }
        }

        if (PHP_VERSION >= 7) {
            $container->setDefinition(
                'biera.test.client',
                new Definition(
                    'Biera\TestBundle\Client',
                    [
                        new Reference('kernel'),
                        new Parameter('test.client.parameters'),
                        new Reference('test.client.history'),
                        new Reference('test.client.cookiejar')
                    ]
                )
            );
        } else {
            $container->addAliases(['biera.test.client' => 'test.client']);
        }
    }
}
