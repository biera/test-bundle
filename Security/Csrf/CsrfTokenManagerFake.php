<?php

namespace Biera\TestBundle\Security\Csrf;

use Symfony\Component\Security\Csrf\CsrfToken;
use Symfony\Component\Security\Csrf\CsrfTokenManagerInterface;

class CsrfTokenManagerFake implements CsrfTokenManagerInterface
{
    /**
     * @param string $tokenId The token ID
     *
     * @return CsrfToken The CSRF token
     */
    public function getToken($tokenId)
    {
        return new CsrfToken($tokenId, '');
    }

    /**
     * @param string $tokenId The token ID.
     *
     * @return CsrfToken The CSRF token
     */
    public function refreshToken($tokenId)
    {
        return $this->getToken($tokenId);
    }

    /**
     * @param string $tokenId The token ID
     *
     * @return null
     */
    public function removeToken($tokenId)
    {
        return null;
    }

    /**
     * @param CsrfToken $token A CSRF token
     *
     * @return bool
     */
    public function isTokenValid(CsrfToken $token)
    {
        return true;
    }
}