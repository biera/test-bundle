<?php
namespace Biera\TestBundle\Assert;

use Symfony\Component\DomCrawler\Crawler;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Profiler\Profile;

/**
 * Common asserts for WebTestCase
 *
 *  TODO : assure that class is derived from WebTestCase!
 *
 * @author Jakub Biernacki <kubiernacki@gmail.com>
 */
trait WebAssert
{
    /**
     * Assert that response status code is 2xx family member
     *
     * @param Response $response
     * @param string|null  $method
     * @param string|null  $uri
     */
    protected function assertResponseIsSuccessful(Response $response, $method = null, $uri = null)
    {
        $this->assertTrue(
            $response->isSuccessful(),
            $method || $uri
                ? sprintf('Status code should be 2xx/3xx, got: %d', $response->getStatusCode())
                : sprintf('Status code of %s %s request should be 2xx or 3xx, got %d', $method, $uri, $response->getStatusCode())
        );
    }

    /**
     * Assert that response status code is 4xx family member
     *
     * @param Response $response
     */
    protected function assertClientError(Response $response)
    {
        $this->assertTrue(
            $response->isClientError(), sprintf('Status code should be 4xx but is %d instead', $response->getStatusCode())
        );
    }

    /**
     * Assert that response body contains given phrase
     *
     * @param Crawler  $crawler
     * @param string   $phrase
     * @param string   $failureMessage
     */
    protected function assertResponseBodyContains(Crawler $crawler, $phrase, $failureMessage)
    {
        $this->assertTrue($crawler->filter('html:contains("' . $phrase . '")')->count() > 0, $failureMessage);
    }

    /**
     * Assert that access is forbidden
     *
     * @param Response $response
     */
    protected function assertAccessIsForbidden(Response $response)
    {
        $this->assertTrue(
            $response->isForbidden(),
            sprintf('Response status should be 403 but is %d', $response->getStatusCode())
        );
    }

    /**
     * Assert that response body does not contain given phrase
     *
     * @param Crawler  $crawler
     * @param string   $phrase
     * @param string   $failureMessage
     */
    protected function assertResponseBodyNotContain(Crawler $crawler, $phrase, $failureMessage)
    {
        $this->assertTrue($crawler->filter('html:contains("' . $phrase . '")')->count() === 0, $failureMessage);
    }

    /**
     * Assert that response body contains given phrase
     *
     * @param Response $response
     */
    protected function assertRequestIsRedirected(Response $response)
    {
        $this->assertTrue($response->isRedirection(), sprintf('Response status code is %d but should be 3xx', $response->getStatusCode()));
    }

    /**
     * Assert that client is authenticated
     *
     * @param Profile $profile
     */
    protected function assertIsAuthenticated(Profile $profile)
    {
        $securityCollector = $profile->getCollector('security');

        $this->assertTrue(
            false === strpos($securityCollector->getTokenClass(), 'AnonymousToken') && $securityCollector->isAuthenticated(),
            sprintf('Client should be authenticated.')
        );
    }

    /**
     * Assert that client is not authenticated
     *
     * @param Profile $profile
     */
    protected function assertIsNotAuthenticated(Profile $profile)
    {
        $securityCollector = $profile->getCollector('security');

        $this->assertTrue(
            false !== strpos($tokenClass = $securityCollector->getTokenClass(), 'AnonymousToken') || !$securityCollector->isAuthenticated(),
            sprintf('Client should not be authenticated. Token class is %s', $tokenClass)
        );
    }

    /**
     * Assert that email has been sent
     *
     * @param Profile $profile
     */
    protected function assertEmailIsSent(Profile $profile)
    {
        /**
         * @var \Symfony\Bundle\SwiftmailerBundle\DataCollector\MessageDataCollector
         */
        $swiftmailerCollector = $profile->getCollector('swiftmailer');

        $this->assertGreaterThan(0, $swiftmailerCollector->getMessageCount(), 'No e-mail has been sent.');
    }
}