<?php

namespace Biera\TestBundle;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Doctrine\Common\DataFixtures\Executor\MongoDBExecutor;
use Doctrine\Common\DataFixtures\Executor\ORMExecutor;
use Doctrine\Common\DataFixtures\Purger\MongoDBPurger;
use Doctrine\Common\DataFixtures\Purger\ORMPurger;


class ExecutorFactory
{
    private $supportedExecutors = ['orm', 'odm'];

    /**
     * e.g:
     * [
     *  'odm' => [
     *      'default' => 'doctrine_mongodb.odm.default_document_manager'
     *  ]
     *  'orm' => [
     *      'custom' => 'service_id_of_custom_orm_entity_manager'
     *  ]
     * ]
     *
     * @var array
     */
    private $executors;

    /**
     * @var ContainerInterface
     */
    private $container;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
        $this->executors = [];
    }

    /**
     * @param string $type
     * @param string $name
     * @param string $objectManagerServiceId
     */
    public function addExecutor($type, $name, $objectManagerServiceId)
    {
        if (!$this->isSupported($type)) {
            throw new \InvalidArgumentException(sprintf('Unsupported executor type: %s', $type));
        }

        if (!array_key_exists($type, $this->executors)) {
            $this->executors[$type] = [];
        }

        $this->executors[$type][$name] = $objectManagerServiceId;
    }

    /**
     * @param string $executorId
     *
     * @return Doctrine\Common\DataFixtures\Executor\AbstractExecutor
     */
    public function create($executorId)
    {
        if (empty($this->executors)) {
            throw new \LogicException('There are no executors. Please, define one!');
        }

        if ('default' === $executorId) {
            // get the very first executor
            $executorType = $this->extractFirstElementKey($this->executors);
            $executorName = $this->extractFirstElementKey($this->executors[$executorType]);
        }  else {
            if (-1 !== preg_match('/^([a-z]+)\.(.+)/', $executorId, $matches)) {
                list($executorType, $executorName) = $matches;
            }  else {
               throw new \InvalidArgumentException('Executor id malformed');
            }
        }

        return $this->createExecutor($executorType, $executorName);
    }

    private function createExecutor($executorType, $executorName)
    {
        if (!$this->isSupported($executorType)) {
            throw new \InvalidArgumentException(sprintf('Unsupported executor type: %s', $executorType));
        }

        if (!array_key_exists($executorType, $this->executors) || !array_key_exists($executorName, $this->executors[$executorType])) {
            throw new \InvalidArgumentException(sprintf('Executor %s.%s not defined', $executorType, $executorName));
        }

        $objectManager = $this->container->get($this->executors[$executorType][$executorName]);

        if ('odm' === $executorType) {
            $executor = new MongoDBExecutor($objectManager, new MongoDBPurger());
        } else {
            $executor = new ORMExecutor($objectManager, new ORMPurger());
        }

        return $executor;
    }

    private function extractFirstElementKey(array $array)
    {
        $keys = array_keys($array);

        return $keys[0];
    }

    /**
     * @param $executorType
     * @return bool
     */
    private function isSupported($executorType)
    {
        return in_array($executorType, $this->supportedExecutors);
    }
}