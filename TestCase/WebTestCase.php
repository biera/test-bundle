<?php
namespace Biera\TestBundle\TestCase;

use Biera\TestBundle\Assert\WebAssert;
use Doctrine\Common\DataFixtures\Executor\AbstractExecutor;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\DataFixtures\Loader;
use Doctrine\Common\DataFixtures\ReferenceRepository;
use LogicException;
use Symfony\Bundle\FrameworkBundle\Client;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase as SymfonyWebTestCase;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\HttpKernel\Profiler\Profile;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * Common code for WebTestCase
 *
 * @author Jakub Biernacki <kubiernacki@gmail.com>
 */
abstract class WebTestCase extends SymfonyWebTestCase
{
    /**
     * Common asserts for web tests
     */
    use WebAssert;

    /**
     * GET
     */
    const HTTP_METHOD_GET = 'GET';

    /**
     * POST
     */
    const HTTP_METHOD_POST = 'POST';

    /**
     * @var Client
     */
    protected $client;

    /**
     * @var boolean
     */
    protected $followRedirects = true;

    /**
     * @var ReferenceRepository
     */
    private static $referenceRepository;

    /**
     * {@inheritdoc}
     */
    protected static function createClient(array $options = array(), array $server = array())
    {
        static::bootKernel($options);

        $client = static::$kernel->getContainer()->get('biera.test.client');
        $client->setServerParameters($server);

        return $client;
    }

    /**
     * Generate URL from route name
     *
     * @param string $route  route's name
     * @param array  $params array of route's params
     */
    protected function generateUrl($route, array $params = [])
    {
        return static::$kernel->getContainer()->get('router')->generate($route, $params, true);
    }


    public static function loadFixtures()
    {
        /**
         * @var FixtureInterface[]
         */
        $fixtures = static::fixtures();

        if (!empty($fixtures)) {

            if (2 == count($fixtures) && is_array($fixtures[1])) {
                list($executorId, $fixtureClasses) = $fixtures;
            } else {
                $fixtureClasses = $fixtures;
            }

            static::bootKernel();

            $executorFactory = static::$kernel->getContainer()->get('biera_test.fixtures_executor_factory');

            /** @var AbstractExecutor */
            $executor = $executorFactory->create(isset($executorId) ? $executorId : 'default');

            self::$referenceRepository = $executor->getReferenceRepository();

            $loader = new Loader();

            foreach ($fixtureClasses as $fixtureClass) {

                if ($fixtureClass instanceof ContainerAwareInterface) {
                    $fixtureClass->setContainer(static::$kernel->getContainer());
                }

                $loader->addFixture($fixtureClass);
            }

            $executor->execute($loader->getFixtures());
        }
    }

    /**
     * Get reference repository
     *
     * @throws LogicException
     *
     * @return ReferenceRepository
     */
    protected function getReferenceRepository()
    {
       if (null === self::$referenceRepository) {
           throw new LogicException(sprintf('It looks like fixtures have not been loaded. Please, implement fixtures() method'));
       }

       return self::$referenceRepository;
    }

    /**
     * Get reference to loaded fixture object
     *
     * @param string $id
     *
     * @return mixed
     */
    protected function getReference($id)
    {
        return $this->getReferenceRepository()->getReference($id);
    }

    protected function doNotFollowRedirectsAndEnableProfiler()
    {
        $this->client->followRedirects(false);
        $this->client->enableProfiler();
    }

    /**
     * {@inheritdoc}
     */
    public function setUp()
    {
        static::loadFixtures();
        $this->client = static::createClient();
        $this->client->followRedirects((boolean) $this->followRedirects);
    }

    /**
     * Get fixtures to load
     *
     * @return array
     */
    protected static function fixtures()
    {
        return [];
    }

    /**
     * @return Profile
     */
    protected function getProfile()
    {
        if (false === $profile = $this->client->getProfile()) {
            throw new \LogicException('Profile must be enabled. Call Symfony\Bundle\FrameworkBundle\Client->enableProfiler() before sending request.');
        }

        return $profile;
    }

    /**
     * Get sent e-mails
     *
     * @param Profile $profile
     *
     * @return array
     */
    protected function getEmails(Profile $profile)
    {
        /**
         * @var \Symfony\Bundle\SwiftmailerBundle\DataCollector\MessageDataCollector
         */
        $swiftMailerCollector = $profile->getCollector('swiftmailer');

        return $swiftMailerCollector->getMessages();
    }

    /**
     * Get first e-mail from collection of sent e-mails
     *
     * @param Profile $profile
     *
     * @return \Swift_Message
     */
    protected function getEmail(Profile $profile)
    {
        /**
         * @var \Symfony\Bundle\SwiftmailerBundle\DataCollector\MessageDataCollector
         */
        $emails = $this->getEmails($profile);

        if (count($emails) === 0) {
            throw new \OutOfBoundsException('No e-mail has been sent.');
        }

        return $emails[0];
    }

    /**
     * Retrieve URI from responses|emails body|etc
     *
     * @param $string
     *
     * @throws \InvalidArgumentException
     *
     * @return strung
     */
    protected function retrieveURI($string)
    {
        // simple URI pattern
        $uriPattern = '/\b(?:(?:https?|ftp):\/\/|www\.)[-a-z0-9+&@#\/%?=~_|!:,.;]*[-a-z0-9+&@#\/%=~_|]/i';

        if (1 !== preg_match($uriPattern, $string, $matches)) {
            throw new \InvalidArgumentException(sprintf('%s does not contain valid url', $string));
        }

        return $matches[0];
    }

    /**
     * Logs in given user entity
     */
//    protected function logIn(UserInterface $user, $firewall)
//    {
//        $session = $this->client->getContainer()->get('session');
//
//        $token = new UsernamePasswordToken($user, null, $firewall, $user->getRoles());
//        $session->set('_security_'.$firewall, serialize($token));
//        $session->save();
//
//        $cookie = new Cookie($session->getName(), $session->getId());
//        $this->client->getCookieJar()->set($cookie);
//        $this->client->getContainer()->get('security.token_storage')->setToken($token);
//    }

}